﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Vouzamo.Common.Extensions;
using Vouzamo.Common.Interop;
using Vouzamo.Common.Plugins;
using Vouzamo.Common.Specification;

namespace Vouzamo.Plugins
{
    public class PluginRepository<T> : IPluginRepository<T> where T : class, IPlugin
    {
        protected DirectoryInfo Directory { get; set; }

        public PluginRepository(string path)
        {
            DirectoryInfo outer = new DirectoryInfo(path);

            if (!outer.Exists)
            {
                outer.Create();
            }

            DirectoryInfo inner = outer.EnumerateDirectories().FirstOrDefault(x => x.Name == typeof(T).Name) ?? outer.CreateSubdirectory(typeof(T).Name);

            if (!inner.Exists)
            {
                inner.Create();
            }

            Directory = inner;
        }

        public ISingleResponse<IConfigurablePlugin<T>> Find(Guid id)
        {
            FileInfo file = Directory.EnumerateFiles().FirstOrDefault(x => x.Name == string.Format("{0}.json", id.ToString()));

            if (file != null)
            {
                string json = File.ReadAllText(file.FullName);
                IConfigurablePlugin<T> value = JsonConvert.DeserializeObject<ConfigurablePlugin<T>>(json);
                return new SingleResponse<IConfigurablePlugin<T>>(value);
            }

            return new SingleResponse<IConfigurablePlugin<T>>();
        }

        public ISingleResponse<IConfigurablePlugin<T>> Create(IConfigurablePlugin<T> entity)
        {
            return Update(entity);
        }

        public ISingleResponse<IConfigurablePlugin<T>> Update(IConfigurablePlugin<T> entity)
        {
            string json = JsonConvert.SerializeObject(entity);
            File.WriteAllText(string.Format("{0}\\{1}.json", Directory.FullName, entity.Id), json);

            return new SingleResponse<IConfigurablePlugin<T>>(entity);
        }

        public ISingleResponse<IConfigurablePlugin<T>> Delete(IConfigurablePlugin<T> entity)
        {
            File.Delete(string.Format("{0}\\{1}.json", Directory.FullName, entity.Id));
            return new SingleResponse<IConfigurablePlugin<T>>();
        }

        protected IQueryable<IConfigurablePlugin<T>> Queryable
        {
            get
            {
                IList<IConfigurablePlugin<T>> all = new List<IConfigurablePlugin<T>>();

                foreach (var file in Directory.GetFiles("*.json"))
                {
                    string json = File.ReadAllText(file.FullName);
                    IConfigurablePlugin<T> value = JsonConvert.DeserializeObject<ConfigurablePlugin<T>>(json);
                    all.Add(value);
                }

                return all.AsQueryable();
            }
        }

        public IManyResponse<IConfigurablePlugin<T>> Query(ISpecification<IConfigurablePlugin<T>> specification)
        {
            var results = Queryable.SatisfiesSpecification(specification).ToList();

            return new ManyResponse<IConfigurablePlugin<T>>(results);
        }

        public IManyResponse<IConfigurablePlugin<T>> Query(IOrderBySpecification<IConfigurablePlugin<T>> specification)
        {
            var results = specification.IsSatisfiedBy(Queryable).ToList();

            return new ManyResponse<IConfigurablePlugin<T>>(results);
        }

        public IManyPagedResponse<IConfigurablePlugin<T>> Query(IOrderBySpecification<IConfigurablePlugin<T>> specification, int resultsPerPage, int page = 1)
        {
            var queryable = specification.IsSatisfiedBy(Queryable);

            int totalResults = queryable.Count();
            int skip = (page - 1) * resultsPerPage;
            int take = resultsPerPage;

            IList<IConfigurablePlugin<T>> results = queryable.Skip(skip).Take(take).ToList();

            return new ManyPagedResponse<IConfigurablePlugin<T>>(results, totalResults, resultsPerPage, page);
        }
    }
}
