using System;
using System.Linq;
using Vouzamo.Common.Specification;

namespace Vouzamo.Blog.Data.Specifications
{
    public class OrderByRandom<T> : IOrderBySpecification<T>
    {
        public IOrderedQueryable<T> IsSatisfiedBy(IQueryable<T> queryable)
        {
            return queryable.OrderBy(x => Guid.NewGuid());
        }
    }
}