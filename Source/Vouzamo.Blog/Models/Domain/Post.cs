﻿using System;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Models.Domain
{
    public class Post : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public DateTime PublishDate { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public Guid AuthorId { get; set; }
        public Guid ImageId { get; set; }

        public Post()
        {
            Id = Guid.NewGuid();
            PublishDate = DateTime.Now;
        }

        public Post(string title, string summary, Guid authorId, Guid imageId, DateTime? publishDate = null) : this()
        {
            Title = title;
            Summary = summary;
            AuthorId = authorId;
            ImageId = imageId;

            if (publishDate.HasValue)
            {
                PublishDate = publishDate.Value;
            }
        }
    }
}