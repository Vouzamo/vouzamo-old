﻿using System.Collections.Generic;

namespace Vouzamo.Patterns.Decorator
{
    public interface IComponent
    {
        
    }

    public interface IComposite<T> where T : IComponent
    {
        IEnumerable<T> Components { get; }
        void Add(T item);
        void Remove(T item);
    }

    public abstract class LibraryItem : IComponent
    {
        public abstract void Borrow(Library library);
        public abstract void Return(Library library);
    }

    public class Book : LibraryItem
    {
        public override void Borrow(Library library)
        {
            library.CheckOut(this);
        }

        public override void Return(Library library)
        {
            library.CheckIn(this);
        }
    }

    public class Movie : LibraryItem
    {
        public override void Borrow(Library library)
        {
            library.CheckOut(this);
        }

        public override void Return(Library library)
        {
            library.CheckIn(this);
        }
    }

    public class CompositeLibraryItem : LibraryItem, IComposite<LibraryItem>
    {
        private readonly IList<LibraryItem> _components = new List<LibraryItem>();

        public IEnumerable<LibraryItem> Components {
            get { return _components; }
        }

        public void Add(LibraryItem item)
        {
            _components.Add(item);
        }

        public void Remove(LibraryItem item)
        {
            _components.Remove(item);
        }

        public override void Borrow(Library library)
        {
            foreach (var item in Components)
            {
                item.Borrow(library);
            }
        }

        public override void Return(Library library)
        {
            foreach (var item in Components)
            {
                item.Return(library);
            }
        }
    }

    public class Library
    {
        private List<LibraryItem> Items { get; set; }

        public Library()
        {
            Items = new List<LibraryItem>();
        }

        public void CheckOut(LibraryItem item)
        {
            Items.Remove(item);
        }

        public void CheckIn(LibraryItem item)
        {
            Items.Add(item);
        }
    }

    public static class Test
    {
        public static void Main()
        {
            Library library = new Library();

            library.CheckIn(new Book());
            library.CheckIn(new Movie());

            CompositeLibraryItem composite = new CompositeLibraryItem();
            composite.Add(new Book());
            composite.Add(new Movie());

            composite.Borrow(library);
        }
    }
}
