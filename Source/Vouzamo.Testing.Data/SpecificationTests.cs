﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Vouzamo.Common.Data;
using Vouzamo.Common.Extensions;
using Vouzamo.Common.Specification;

namespace Vouzamo.Testing.Data
{
    [TestFixture]
    public class SpecificationTests
    {
        protected IList<Entity> Entities { get; set; }

        public SpecificationTests()
        {
            Entities = new List<Entity>
            {
                new Entity("A", 1),
                new Entity("A", 2),
                new Entity("A", 3),
                new Entity("B", 1),
                new Entity("B", 2),
                new Entity("B", 3),
                new Entity("C", 1),
                new Entity("C", 2),
                new Entity("C", 3)
            };


        }

        [Test]
        public void AndSpecification()
        {
            var specificationAAnd1 = new LetterSpecification("A").And(new NumberSpecification(1));
            var queryable = Entities.AsQueryable().SatisfiesSpecification(specificationAAnd1);
            Assert.AreEqual(1, queryable.Count());
        }

        [Test]
        public void OrSpecification()
        {
            var specificationBor2 = new LetterSpecification("B").Or(new NumberSpecification(2));
            var queryable = Entities.AsQueryable().SatisfiesSpecification(specificationBor2);
            Assert.AreEqual(5, queryable.Count());
        }

        [Test]
        public void NotSpecification()
        {
            var specificationNotC = new LetterSpecification("A").Not();
            var queryable = Entities.AsQueryable().SatisfiesSpecification(specificationNotC);
            Assert.AreEqual(6, queryable.Count());
        }

        [Test]
        public void NotAndNotSpecification()
        {
            var specificationNotAAndNot1 = new LetterSpecification("A").Not().And(new NumberSpecification(1).Not());
            var queryable = Entities.AsQueryable().SatisfiesSpecification(specificationNotAAndNot1);
            Assert.AreEqual(4, queryable.Count());
        }

        [Test]
        public void NotOrNotSpecification()
        {
            var specificationNotBOrNot2 = new LetterSpecification("B").Not().Or(new NumberSpecification(2).Not());
            var queryable = Entities.AsQueryable().SatisfiesSpecification(specificationNotBOrNot2);
            Assert.AreEqual(8, queryable.Count());
        }

        [Test]
        public void AndOrAndOrAndSpecification()
        {
            var specificationAAnd1OrBAnd2OrCAnd3 = new LetterSpecification("A").And(new NumberSpecification(1)).Or(new LetterSpecification("B").And(new NumberSpecification(2))).Or(new LetterSpecification("C").And(new NumberSpecification(3)));
            var queryable = Entities.AsQueryable().SatisfiesSpecification(specificationAAnd1OrBAnd2OrCAnd3);
            Assert.AreEqual(3, queryable.Count());
        }
    }

    public class Entity : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Letter { get; set; }
        public int Number { get; set; }
        public DateTime Created { get; set; }

        public Entity()
        {
            Id = Guid.NewGuid();
            Created = DateTime.Now;
        }

        public Entity(string letter, int number)
        {
            Letter = letter;
            Number = number;
        }
    }

    public class LetterSpecification : Specification<Entity>
    {
        public LetterSpecification(string letter)
        {
            Predicate = x => x.Letter == letter;
        }
    }

    public class NumberSpecification : Specification<Entity>
    {
        public NumberSpecification(int number)
        {
            Predicate = x => x.Number == number;
        }
    }
}
