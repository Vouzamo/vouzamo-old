﻿using System;
using System.Web.Mvc;
using Vouzamo.Blog.Data.Queries;
using Vouzamo.Blog.Data.Specifications;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Data;
using Vouzamo.Common.Interop;
using Vouzamo.Common.Specification;

namespace Vouzamo.Blog.Controllers
{
    public class ComponentController : Controller
    {
        protected IQueryDispatcher QueryDispatcher { get; set; }

        public ComponentController(IQueryDispatcher queryDispatcher)
        {
            QueryDispatcher = queryDispatcher;
        }

        public ActionResult Posts()
        {
            var request = QueryDispatcher.Invoke(new AllPostsQuery(1, 12));

            return View(request);
        }

        public ActionResult Witticism()
        {
            var query = new SingleResponse<Witticism>(null, false);

            if (query.Successful)
            {
                return View(query.Response);
            }

            return new EmptyResult();
        }

        public ActionResult Author(Guid id)
        {
            var query = QueryDispatcher.Invoke(new AuthorByIdQuery(id));

            return View(query.Response);
        }

        public ActionResult Sections(Guid id)
        {
            var query = QueryDispatcher.Invoke(new AllSectionsQuery(id));

            return View(query);
        }

        public ActionResult BackgroundImage(Guid id)
        {
            var query = QueryDispatcher.Invoke(new ImageByIdQuery(id));

            return View(query.Response);
        }

        public ActionResult Image(Guid id)
        {
            var query = QueryDispatcher.Invoke(new ImageByIdQuery(id));

            return View(query.Response);
        }
    }
}