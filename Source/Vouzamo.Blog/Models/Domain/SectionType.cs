﻿namespace Vouzamo.Blog.Models.Domain
{
    public enum SectionTypes
    {
        Code,
        Heading,
        Image,
        Quote,
        Text
    }
}