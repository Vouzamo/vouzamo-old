using System;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Models.Domain
{
    public abstract class Section : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid PostId { get; set; }
        public int Order { get; set; }
        public SectionTypes SectionType { get; protected set; }

        protected Section()
        {
            Id = Guid.NewGuid();
            Order = 0;
        }

        protected Section(Guid postId, int order = 0) : this()
        {
            PostId = postId;
            Order = order;
        }
    }

    public class HeadingSection : Section
    {
        public string Heading { get; set; }

        public HeadingSection()
        {
            SectionType = SectionTypes.Heading;
        }

        public HeadingSection(Guid postId, int order, string heading) : base(postId, order)
        {
            SectionType = SectionTypes.Heading;
            Heading = heading;
        }
    }

    public class TextSection : Section
    {
        public string Text { get; set; }

        public TextSection()
        {
            SectionType = SectionTypes.Text;
        }

        public TextSection(Guid postId, int order, string text) : base(postId, order)
        {
            SectionType = SectionTypes.Text;
            Text = text;
        }
    }

    public class CodeSection : Section
    {
        public bool ShowLineNumbers { get; set; }
        public string Code { get; set; }

        public CodeSection()
        {
            SectionType = SectionTypes.Code;
        }

        public CodeSection(Guid postId, int order, string code, bool showLineNumbers = false) : base(postId, order)
        {
            SectionType = SectionTypes.Code;
            Code = code;
            ShowLineNumbers = showLineNumbers;
        }
    }

    public class QuoteSection : Section
    {
        public string Quote { get; set; }
        public string Author { get; set; }
        public string Source { get; set; }

        public QuoteSection()
        {
            SectionType = SectionTypes.Quote;
        }

        public QuoteSection(Guid postId, int order, string quote, string author = null, string source = null) : base(postId, order)
        {
            SectionType = SectionTypes.Quote;
            Quote = quote;
            Author = author;
            Source = source;
        }
    }

    public class ImageSection : Section
    {
        public Image Image { get; set; }

        public ImageSection()
        {
            SectionType = SectionTypes.Image;
        }

        public ImageSection(Guid postId, int order, Image image) : base(postId, order)
        {
            SectionType = SectionTypes.Image;
            Image = image;
        }
    }
}