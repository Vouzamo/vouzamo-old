﻿using System;
using System.Data.Entity;
using Vouzamo.Command.Models;

namespace Vouzamo.EntityFramework.Models.Handlers
{
    public class DeleteCommandHandler<TEntity> : ICommandHandler<DeleteCommand<TEntity>, bool> where TEntity : class
    {
        private readonly DbContext _context;

        public DeleteCommandHandler(DbContext context)
        {
            _context = context;
        }

        public bool Handle(DeleteCommand<TEntity> command)
        {
            try
            {
                TEntity entity = _context.Set<TEntity>().Find(command.Id);

                _context.Entry(entity).State = EntityState.Deleted;
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
