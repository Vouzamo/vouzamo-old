﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using Vouzamo.Common.Plugins;

namespace Vouzamo.Plugins
{
    public class PluginFactory<T> : IPluginFactory<T> where T : class, IPlugin
    {
        [ImportMany(typeof(IPlugin))]
        protected IEnumerable<Lazy<T, IPluginMetadata>> Modules { get; set; }
        protected IList<IConfigurablePlugin<T>> ConfigurableModules { get; set; }

        protected IPluginRepository<T> ConfigurablePluginRepository { get; set; }

        public PluginFactory(IPluginRepository<T> configurablePluginRepository, IEnumerable<string> paths)
        {
            ConfigurablePluginRepository = configurablePluginRepository;

            ConfigurableModules = new List<IConfigurablePlugin<T>>();
            Import(paths);
            MakeImportedModulesConfigurable();
        }

        protected void Import(IEnumerable<string> assemblyPaths)
        {
            var catalog = new AggregateCatalog();

            foreach (string path in assemblyPaths)
            {
                catalog.Catalogs.Add(new DirectoryCatalog(path, "*.dll"));
            }

            AvailabilityExportProvider publicExports = new AvailabilityExportProvider(PluginAvailability.Public);
            AvailabilityExportProvider privateExports = new AvailabilityExportProvider(PluginAvailability.Private);

            var publicContainer = new CompositionContainer(catalog, publicExports);
            var privateContainer = new CompositionContainer(catalog, publicExports, privateExports);

            publicExports.SourceProvider = publicContainer;
            privateExports.SourceProvider = publicContainer;

            privateContainer.SatisfyImportsOnce(this);
        }

        protected void MakeImportedModulesConfigurable()
        {
            ConfigurableModules.Clear();

            foreach (var module in Modules)
            {
                IConfigurablePlugin<T> configurablePlugin;

                var response = ConfigurablePluginRepository.Find(new Guid(module.Metadata.Id));

                if (response.Successful)
                {
                    configurablePlugin = response.Response;
                    configurablePlugin.Configure(module.Metadata);
                }
                else
                {
                    configurablePlugin = new ConfigurablePlugin<T>();
                    configurablePlugin.Configure(module.Metadata);

                    ConfigurablePluginRepository.Create(configurablePlugin);
                }

                if (configurablePlugin.IsEnabled)
                {
                    configurablePlugin.Initialize(module.Value);
                }

                ConfigurableModules.Add(configurablePlugin);
            }
        }

        public IEnumerable<IConfigurablePlugin<T>> All()
        {
            return ConfigurableModules.Where(x => x.IsEnabled);
        }

        public IConfigurablePlugin<T> Default()
        {
            return All().SingleOrDefault(x => x.IsDefault);
        }

        public void Dispose()
        {
            foreach (var plugin in ConfigurableModules)
            {
                // Update configuration
                ConfigurablePluginRepository.Update(plugin);

                // Dispose
                if (plugin.Module != null)
                {
                    plugin.Module.Dispose();
                }
            }
        }
    }
}
