using System;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Specification;

namespace Vouzamo.Blog.Data.Specifications
{
    public class PostSections : Specification<Section>
    {
        protected Guid PostId { get; set; }

        public PostSections(Guid postId)
        {
            Predicate = x => x.PostId == postId;
        }
    }
}