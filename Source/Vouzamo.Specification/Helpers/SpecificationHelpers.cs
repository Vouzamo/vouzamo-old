﻿using Vouzamo.Specification.Models;
using Vouzamo.Specification.Models.Interfaces;

namespace Vouzamo.Specification.Helpers
{
    public static class SpecificationHelpers
    {
        public static ISpecification<T> And<T>(this ISpecification<T> left, ISpecification<T> right)
        {
            return new AndSpecification<T>(left, right);
        }

        public static ISpecification<T> Or<T>(this ISpecification<T> left, ISpecification<T> right)
        {
            return new OrSpecification<T>(left, right);
        }
    }
}
