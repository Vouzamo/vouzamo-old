﻿using System;
using System.Data.Entity;
using Vouzamo.Command.Models;
using Vouzamo.Common.Interop;
using Vouzamo.EntityFramework.Models.Interfaces;

namespace Vouzamo.EntityFramework.Models.Handlers
{
    public class GetCommandHandler<TEntity, TViewModel> : ICommandHandler<GetCommand<TEntity, TViewModel>, TViewModel> where TEntity : class where TViewModel : IEditable<TEntity>, new()
    {
        private readonly IMapper _mapper;
        private readonly DbContext _context;

        public GetCommandHandler(IMapper mapper, DbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public TViewModel Handle(GetCommand<TEntity, TViewModel> command)
        {
            try
            {
                // Persistance
                TEntity entity = _context.Set<TEntity>().Find(command.Id);

                // Mapping
                TViewModel model = _mapper.Map<TEntity, TViewModel>(entity);

                return model;
            }
            catch (Exception ex)
            {
                return default(TViewModel);
            }
        }
    }
}
