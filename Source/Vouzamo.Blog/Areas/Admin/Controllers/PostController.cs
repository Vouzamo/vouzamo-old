﻿using System;
using System.Web.Mvc;
using Vouzamo.Blog.Data.Commands;
using Vouzamo.Blog.Data.Queries;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Areas.Admin.Controllers
{
    public class PostController : Controller
    {
        protected IQueryDispatcher QueryDispatcher { get; set; }
        protected ICommandDispatcher CommandDispatcher { get; set; }

        public PostController(IQueryDispatcher queryDispatcher, ICommandDispatcher commandDispatcher)
        {
            QueryDispatcher = queryDispatcher;
            CommandDispatcher = commandDispatcher;
        }

        public ActionResult List(int id =  1)
        {
            var response = QueryDispatcher.Invoke(new AllPostsQuery(id, 12));

            return View(response);
        }

        public ActionResult Create()
        {
            return View(new Post());
        }

        [HttpPost]
        public ActionResult Create(CreatePostCommand model)
        {
            if (ModelState.IsValid)
            {
                var response = CommandDispatcher.Invoke(model);

                return RedirectToAction("List");
            }

            return View(model);
        }

        public ActionResult Edit(Guid id)
        {
            var response = QueryDispatcher.Invoke(new PostByIdQuery(id));

            if (response.Successful)
            {
                return View(response.Response);
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Edit(Post model)
        {
            if (ModelState.IsValid)
            {
                //var response = CommandDispatcher.Invoke(new ChangePostCommand());

                return RedirectToAction("List");
            }

            return View(model);
        }
    }
}