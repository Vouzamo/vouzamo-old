using Vouzamo.Blog.Models.Domain;

namespace Vouzamo.Blog.Data.Specifications
{
    public class AllAuthorsSpecification : AllSpecification<Author>
    {

    }
}