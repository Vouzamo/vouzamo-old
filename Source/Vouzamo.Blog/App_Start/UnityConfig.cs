﻿using System;
using System.Reflection;
using Microsoft.Practices.Unity;
using SimpleInjector;
using Vouzamo.Blog.Dispatchers;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // UnitOfWork
            //container.RegisterType<IFactory<IUnitOfWork>, UnitOfWorkFactory>(new PerRequestLifetimeManager());

            // Register all command handlers and query handlers

            // Command
            container.RegisterType<ICommandDispatcher, UnityCommandDispatcher>(new PerRequestLifetimeManager(), new InjectionMember[] { new InjectionConstructor(container) });

            // Query
            container.RegisterType<IQueryDispatcher, UnityQueryDispatcher>(new PerRequestLifetimeManager(), new InjectionMember[] { new InjectionConstructor(container) });
        }
    }

    public class SimpleInjectorConfig
    {
        public static void RegisterTypes(Container container)
        {
            container.Register(typeof(ICommandHandler<,>), new [] { Assembly.GetExecutingAssembly() });
            container.Register(typeof(IQueryHandler<,>), new [] { Assembly.GetExecutingAssembly() });

            container.Register<ICommandDispatcher>(() => new SimpleInjectorCommandDispatcher(container));
            container.Register<IQueryDispatcher>(() => new SimpleInjectorQueryDispatcher(container));
        }
    }
}
