﻿using Vouzamo.Common.Specification;

namespace Vouzamo.Blog.Data.Specifications
{
    public class AllSpecification<T> : Specification<T>
    {
        public AllSpecification()
        {
            Predicate = x => true;
        }
    }
}