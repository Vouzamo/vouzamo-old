﻿using System;
using System.Web.Mvc;
using Vouzamo.Blog.Data.Queries;
using Vouzamo.Blog.Data.Specifications;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Data;
using Vouzamo.Common.Interop;
using Vouzamo.Common.Specification;

namespace Vouzamo.Blog.Areas.Admin.Controllers
{
    public class SectionController : Controller
    {
        protected IQueryDispatcher QueryDispatcher { get; set; }

        public SectionController(IQueryDispatcher queryDispatcher)
        {
            QueryDispatcher = queryDispatcher;
        }

        public ActionResult List(Guid id)
        {
            var request = QueryDispatcher.Invoke(new AllSectionsQuery(id));

            ViewBag.PostId = id;

            return View(request.Response);
        }

        public ActionResult CreateTextSection(Guid id)
        {
            var request = QueryDispatcher.Invoke(new PostByIdQuery(id));

            if (request.Successful)
            {
                return View(new TextSection() { PostId = id });
            }

            return HttpNotFound();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CreateTextSection(TextSection model)
        {
            //if (ModelState.IsValid)
            //{
            //    using (var uow = UoWFactory.Create())
            //    {
            //        uow.Repository<Section, Guid>().Create(model);
            //        uow.Commit();
            //    }

            //    return RedirectToAction("Edit", new { id = model.Id });
            //}

            return View(model);
        }
    }
}