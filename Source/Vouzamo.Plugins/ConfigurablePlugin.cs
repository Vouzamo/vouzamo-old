﻿using System;
using Newtonsoft.Json;
using Vouzamo.Common.Plugins;

namespace Vouzamo.Plugins
{
    public class ConfigurablePlugin<T> : IConfigurablePlugin<T> where T : class, IPlugin
    {
        [JsonIgnore]
        protected T _module;

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonIgnore]
        public T Module
        {
            get { return IsEnabled ? _module : default(T); }
            protected set { _module = value; }
        }
        [JsonIgnore]
        public IPluginMetadata Metadata { get; set; }
        [JsonProperty("enabled")]
        public bool IsEnabled { get; set; }
        [JsonProperty("default")]
        public bool IsDefault { get; set; }

        public ConfigurablePlugin()
        {
            Id = Guid.NewGuid();

            IsEnabled = false;
            IsDefault = false;
            _module = default(T);
        }

        public void Configure(IPluginMetadata metadata)
        {
            Id = new Guid(metadata.Id);
            Metadata = metadata;
        }

        public void Initialize(T module)
        {
            _module = module;
        }
    }
}
