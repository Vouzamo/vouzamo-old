﻿using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using Vouzamo.Common.Plugins;

namespace Vouzamo.Plugins
{
    public class AvailabilityExportProvider : ComposablePartExportProvider
    {
        private PluginAvailability Availability { get; set; }

        public AvailabilityExportProvider(PluginAvailability availability)
        {
            Availability = availability;
        }

        protected override IEnumerable<Export> GetExportsCore(ImportDefinition definition, AtomicComposition atomicComposition)
        {
            var exports = base.GetExportsCore(definition, atomicComposition);

            if (exports != null)
            {
                return exports.Where(x => x.Metadata.ContainsKey("Availability") && Equals(x.Metadata["Availability"], Availability.ToString()));
            }

            return Enumerable.Empty<Export>();
        }
    }
}
