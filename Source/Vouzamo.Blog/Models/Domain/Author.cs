﻿using System;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Models.Domain
{
    public class Author : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Author()
        {
            Id = Guid.NewGuid();
        }

        public Author(string firstName, string lastName) : this()
        {
            FirstName = firstName;
            LastName = lastName;
        }
    }
}