using System;
using Microsoft.Practices.Unity;
using SimpleInjector;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Dispatchers
{
    public class SimpleInjectorCommandDispatcher : ICommandDispatcher
    {
        protected Container Container { get; set; }

        public SimpleInjectorCommandDispatcher(Container container)
        {
            Container = container;
        }

        public TResult Invoke<TResult>(ICommand<TResult> command)
        {
            Type commandHandlerType = typeof(ICommandHandler<,>).MakeGenericType(command.GetType(), typeof(TResult));

            // Resolve
            dynamic handler = Container.GetInstance(commandHandlerType);

            // Execute
            return handler.Execute((dynamic)command);
        }
    }

    public class UnityCommandDispatcher : ICommandDispatcher
    {
        protected IUnityContainer Container { get; set; }

        public UnityCommandDispatcher(IUnityContainer container)
        {
            Container = container;
        }

        public TResult Invoke<TResult>(ICommand<TResult> command)
        {
            // Resolve
            ICommandHandler<ICommand<TResult>, TResult> handler = Container.Resolve<ICommandHandler<ICommand<TResult>, TResult>>();

            // Execute
            return handler.Execute(command);
        }
    }
}