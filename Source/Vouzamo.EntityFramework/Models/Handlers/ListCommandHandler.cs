﻿using System.Data.Entity;
using Vouzamo.Command.Models;
using Vouzamo.Common.Interop;
using Vouzamo.EntityFramework.Helpers;
using Vouzamo.EntityFramework.Models.Interfaces;
using Vouzamo.Pagination.Models.Interfaces;

namespace Vouzamo.EntityFramework.Models.Handlers
{
    public class ListCommandHandler<TEntity, TViewModel> : ICommandHandler<ListCommand<TEntity, TViewModel>, SearchResultModel<TEntity, TViewModel>> where TEntity : class where TViewModel : IEditable<TEntity>, new()
    {
        private readonly IMapper _mapper;
        private readonly DbContext _context;

        public ListCommandHandler(IMapper mapper, DbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public SearchResultModel<TEntity, TViewModel> Handle(ListCommand<TEntity, TViewModel> command)
        {
            var specification = new TViewModel().SearchSpecification(command.Keyword);

            return new SearchResultModel<TEntity, TViewModel>(_mapper, command.Keyword, _context.Set<TEntity>().PaginatedQueryBySpecification(specification, command.ResultsPerPage, command.Page));
        }
    }
}
