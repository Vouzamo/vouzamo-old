﻿using System.Linq;

namespace Vouzamo.Specification.Models.Interfaces
{
    public interface IOrderBySpecification<T>
    {
        IOrderedQueryable<T> SatisfiesMany(IQueryable<T> queryable);
    }
}