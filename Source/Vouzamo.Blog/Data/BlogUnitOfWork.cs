﻿using System.Data.Entity;
using Vouzamo.Blog.Models.Domain;

namespace Vouzamo.Blog.Data
{
    public class BlogContext : DbContext
    {
        public DbSet<Image> Images { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Witticism> Witticisms { get; set; }
        public DbSet<Section> Sections { get; set; }

        public BlogContext()
        {
            Database.SetInitializer(new BlogInitializer());
        }
    }

    //public class UnitOfWorkFactory : IFactory<IUnitOfWork>
    //{
    //    public IUnitOfWork Create()
    //    {
    //        return new BlogContext();
    //    }
    //}
}