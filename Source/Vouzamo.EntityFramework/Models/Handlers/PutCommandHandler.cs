﻿using System;
using System.Data.Entity;
using Vouzamo.Command.Models;
using Vouzamo.Common.Interop;
using Vouzamo.EntityFramework.Models.Interfaces;

namespace Vouzamo.EntityFramework.Models.Handlers
{
    public class PutCommandHandler<TEntity, TViewModel> : ICommandHandler<PutCommand<TEntity, TViewModel>, TViewModel> where TEntity : class where TViewModel : IEditable<TEntity>, new()
    {
        private readonly IMapper _mapper;
        private readonly DbContext _context;

        public PutCommandHandler(IMapper mapper, DbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public TViewModel Handle(PutCommand<TEntity, TViewModel> command)
        {
            try
            {
                // Assign Id
                var model = command.Model;
                model.Id = command.Id;

                // Mapping
                TEntity entity = _mapper.Map<TViewModel, TEntity>(model);

                // Persistance
                _context.Entry(entity).State = EntityState.Modified;
                _context.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                return default(TViewModel);
            }
        }
    }
}
