﻿using System;
using System.Data.Entity;
using Vouzamo.Blog.Data.Specifications;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Data;
using Vouzamo.Common.Extensions;
using Vouzamo.Common.Interop;
using Vouzamo.Common.Specification;
using Vouzamo.Data.EntityFramework;

namespace Vouzamo.Blog.Data.Queries
{
    public class AllPostsQuery : IQuery<IManyPagedResponse<Post>>
    {
        public int Page { get; protected set; }
        public int ResultsPerPage { get; protected set; }

        public AllPostsQuery(int page, int resultsPerPage)
        {
            Page = page;
            ResultsPerPage = resultsPerPage;
        }
    }

    public class AllPostsQueryHandler : EntityFrameworkQueryHandler<Post>, IQueryHandler<AllPostsQuery, IManyPagedResponse<Post>>
    {
        public AllPostsQueryHandler(DbContext context) : base(context)
        {
            
        }

        public IManyPagedResponse<Post> Execute(AllPostsQuery query)
        {
            var specification = new AllSpecification<Post>().OrderByDescending(x => x.PublishDate);
            var results = Data.SatisfiesSpecification(specification);

            return results.ToManyPagedResponse(query.Page, query.ResultsPerPage);
        }
    }

    public class AllSectionsQuery : IQuery<IManyResponse<Section>>
    {
        public Guid PostId { get; protected set; }

        public AllSectionsQuery(Guid postId)
        {
            PostId = postId;
        }
    }

    public class AllSectionsQueryHandler : EntityFrameworkQueryHandler<Section>, IQueryHandler<AllSectionsQuery, IManyResponse<Section>>
    {
        public AllSectionsQueryHandler(DbContext context) : base(context)
        {
            
        }

        public IManyResponse<Section> Execute(AllSectionsQuery query)
        {
            var specification = new PostSections(query.PostId);
            var results = Data.SatisfiesSpecification(specification);

            return results.ToManyReponse();
        }
    }

    public class PostsByAuthorIdQuery : IQuery<IManyPagedResponse<Post>>
    {
        public Guid AuthorId { get; protected set; }
        public int Page { get; protected set; }
        public int ResultsPerPage { get; protected set; }

        public PostsByAuthorIdQuery(Guid authorId, int page, int resultsPerPage)
        {
            AuthorId = authorId;
            Page = page;
            ResultsPerPage = resultsPerPage;
        }
    }

    public class PostsByAuthorIdQueryHandler : EntityFrameworkQueryHandler<Post>, IQueryHandler<PostsByAuthorIdQuery, IManyPagedResponse<Post>>
    {
        public PostsByAuthorIdQueryHandler(DbContext context) : base(context)
        {
            
        }

        public IManyPagedResponse<Post> Execute(PostsByAuthorIdQuery query)
        {
            var specification = new AuthorPosts(query.AuthorId).OrderByDescending(x => x.PublishDate);
            var results = Data.SatisfiesSpecification(specification);

            return results.ToManyPagedResponse(query.Page, query.ResultsPerPage);
        }
    }

    public class PostByIdQuery : IQuery<ISingleResponse<Post>>
    {
        public Guid PostId { get; protected set; }

        public PostByIdQuery(Guid postId)
        {
            PostId = postId;
        }
    }

    public class PostByIdQueryHandler : EntityFrameworkQueryHandler<Post>, IQueryHandler<PostByIdQuery, ISingleResponse<Post>>
    {
        public PostByIdQueryHandler(DbContext context) : base(context)
        {
            
        }

        public ISingleResponse<Post> Execute(PostByIdQuery query)
        {
            var specification = new IdentifierSpecification<Post, Guid>(x => x.Id, query.PostId);
            var results = Data.SatisfiesSpecification(specification);
            
            return results.ToSingleReponse();
        }
    }

    public class AllAuthorsQuery : IQuery<IManyPagedResponse<Author>>
    {
        public int Page { get; protected set; }
        public int ResultsPerPage { get; protected set; }
    }

    public class AllAuthorsQueryHandler : EntityFrameworkQueryHandler<Author>, IQueryHandler<AllAuthorsQuery, IManyPagedResponse<Author>>
    {
        public AllAuthorsQueryHandler(DbContext context) : base(context)
        {

        }

        public IManyPagedResponse<Author> Execute(AllAuthorsQuery query)
        {
            var specification = new AllSpecification<Author>().OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
            var results = Data.SatisfiesSpecification(specification);
            
            return results.ToManyPagedResponse(query.Page, query.ResultsPerPage);
        }
    }

    public class AuthorByIdQuery : IQuery<ISingleResponse<Author>>
    {
        public Guid AuthorId { get; protected set; }

        public AuthorByIdQuery(Guid authorId)
        {
            AuthorId = authorId;
        }
    }

    public class AuthorByIdQueryHandler : EntityFrameworkQueryHandler<Author>, IQueryHandler<AuthorByIdQuery, ISingleResponse<Author>>
    {
        public AuthorByIdQueryHandler(DbContext context) : base(context)
        {

        }

        public ISingleResponse<Author> Execute(AuthorByIdQuery query)
        {
            var specification = new IdentifierSpecification<Author, Guid>(x => x.Id, query.AuthorId);
            var results = Data.SatisfiesSpecification(specification);
            
            return results.ToSingleReponse();
        }
    }

    public class AuthorByPostIdQuery : IQuery<ISingleResponse<Author>>
    {
        public Guid PostId { get; protected set; }

        public AuthorByPostIdQuery(Guid postId)
        {
            PostId = postId;
        }
    }

    public class AuthorByPostIdQueryHandler : EntityFrameworkQueryHandler<Post>, IQueryHandler<AuthorByPostIdQuery, ISingleResponse<Author>>
    {
        private IQueryDispatcher QueryDispatcher { get; set; }

        public AuthorByPostIdQueryHandler(IQueryDispatcher queryDispatcher, DbContext context) : base(context)
        {
            QueryDispatcher = queryDispatcher;
        }

        public ISingleResponse<Author> Execute(AuthorByPostIdQuery query)
        {
            var specification = new IdentifierSpecification<Post, Guid>(x => x.Id, query.PostId);
            var results = Data.SatisfiesSpecification(specification);
            var response = results.ToSingleReponse();

            if (!response.Successful)
            {
                return new SingleResponse<Author>(default(Author), false, response.Exceptions);
            }

            Guid authorId = response.Response.AuthorId;

            return QueryDispatcher.Invoke(new AuthorByIdQuery(authorId));
        }
    }

    public class AllImagesQuery : IQuery<IManyPagedResponse<Image>>
    {
        public int Page { get; protected set; }
        public int ResultsPerPage { get; protected set; }

        public AllImagesQuery(int page, int resultsPerPage)
        {
            Page = page;
            ResultsPerPage = resultsPerPage;
        }
    }

    public class AllImagesQueryHandler : EntityFrameworkQueryHandler<Image>, IQueryHandler<AllImagesQuery, IManyPagedResponse<Image>>
    {
        public AllImagesQueryHandler(DbContext context) : base(context)
        {

        }

        public IManyPagedResponse<Image> Execute(AllImagesQuery query)
        {
            var specification = new AllSpecification<Image>().OrderBy(x => x.AlternateText);

            var results = Data.SatisfiesSpecification(specification);

            return results.ToManyPagedResponse(query.Page, query.ResultsPerPage);
        }
    }

    public class ImageByIdQuery : IQuery<ISingleResponse<Image>>
    {
        public Guid ImageId { get; protected set; }

        public ImageByIdQuery(Guid imageId)
        {
            ImageId = imageId;
        }
    }

    public class ImageByIdQueryHandler : EntityFrameworkQueryHandler<Image>, IQueryHandler<ImageByIdQuery, ISingleResponse<Image>>
    {
        public ImageByIdQueryHandler(DbContext context) : base(context)
        {

        }

        public ISingleResponse<Image> Execute(ImageByIdQuery query)
        {
            var specification = new IdentifierSpecification<Image, Guid>(x => x.Id, query.ImageId);
            var results = Data.SatisfiesSpecification(specification);

            return results.ToSingleReponse();
        }
    }

    public class ImageByPostIdQuery : IQuery<ISingleResponse<Image>>
    {
        public Guid PostId { get; protected set; }

        public ImageByPostIdQuery(Guid postId)
        {
            PostId = postId;
        }
    }

    public class ImageByPostIdQueryHandler : EntityFrameworkQueryHandler<Post>, IQueryHandler<ImageByPostIdQuery, ISingleResponse<Image>>
    {
        private IQueryDispatcher QueryDispatcher { get; set; }

        public ImageByPostIdQueryHandler(IQueryDispatcher queryDispatcher, DbContext context) : base(context)
        {
            QueryDispatcher = queryDispatcher;
        }

        public ISingleResponse<Image> Execute(ImageByPostIdQuery query)
        {
            var specification = new IdentifierSpecification<Post, Guid>(x => x.Id, query.PostId);
            var results = Data.SatisfiesSpecification(specification);
            var response = results.ToSingleReponse();

            if (!response.Successful)
            {
                return new SingleResponse<Image>(default(Image), false, response.Exceptions);
            }

            Guid imageId = response.Response.ImageId;

            return QueryDispatcher.Invoke(new ImageByIdQuery(imageId));
        }
    }
}