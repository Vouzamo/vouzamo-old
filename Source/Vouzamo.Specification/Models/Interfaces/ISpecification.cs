﻿namespace Vouzamo.Specification.Models.Interfaces
{
    public interface ISpecification<in T>
    {
        bool IsSatisfiedBy(T subject);
    }
}
