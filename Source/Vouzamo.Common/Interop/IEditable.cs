﻿using System;

namespace Vouzamo.Common.Interop
{
    public interface IEditable
    {
        Guid Id { get; set; }
        string Name { get; }
    }
}
