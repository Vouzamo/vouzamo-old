﻿namespace Vouzamo.Common.Interop
{
    public interface ISearch
    {
        string Keyword { get; }
    }
}
