﻿namespace Vouzamo.Common.Interop
{
    public interface IMapper
    {
        TDestination Map<TSource, TDestination>(TSource toBeMapped);
    }
}