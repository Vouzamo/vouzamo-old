﻿namespace Vouzamo.Patterns.Builder
{
    /// <summary>
    /// Identifies a class that implements a director in the builder pattern
    /// </summary>
    /// <typeparam name="TBuilder">The type of builder that this director can construct</typeparam>
    /// <typeparam name="T">The type of object to build</typeparam>
    public interface IDirector<in TBuilder, out T> where TBuilder : IBuilder<T>
    {
        /// <summary>
        /// Constructs using the given builder
        /// </summary>
        /// <param name="builder">The builder to construct</param>
        T Construct(TBuilder builder);
    }
}