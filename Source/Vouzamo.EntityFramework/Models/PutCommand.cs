﻿using System;
using Vouzamo.Command.Models;
using Vouzamo.EntityFramework.Models.Interfaces;

namespace Vouzamo.EntityFramework.Models
{
    public class PutCommand<TEntity, TViewModel> : ICommand<TViewModel> where TViewModel : IEditable<TEntity>, new()
    {
        public Guid Id { get; protected set; }
        public TViewModel Model { get; protected set; }

        public PutCommand(Guid id, TViewModel model)
        {
            Id = id;
            Model = model;
        }
    }
}