﻿using System;
using System.Collections.Generic;
using Vouzamo.Pagination.Models.Interfaces;

namespace Vouzamo.Pagination.Models
{
    /// <summary>
    /// Concrete type for paged results
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class PagedResults<TResult> : IPagination
    {
        #region Assigned Properties
        public IEnumerable<TResult> Results { get; private set; }
        public int TotalResults { get; private set; }
        public int ResultsPerPage { get; private set; }
        public int Page { get; private set; }
        #endregion

        #region Calculated Properties
        public int TotalPages
        {
            get
            {
                int totalPages = TotalResults / ResultsPerPage;

                if (TotalResults % ResultsPerPage > 0)
                {
                    totalPages += 1;
                }

                return totalPages;
            }
        }
        public int PreviousPage { get { return Math.Max(1, Page - 1); } }
        public int NextPage { get { return Math.Min(TotalPages, Page + 1); } }
        public bool HasPrevious { get { return Page > 1; } }
        public bool HasNext { get { return TotalPages > Page; } }
        #endregion

        public PagedResults(IEnumerable<TResult> results, int totalResults, int resultsPerPage, int page)
        {
            Results = results;
            TotalResults = totalResults;
            ResultsPerPage = resultsPerPage;
            Page = page;
        }
    }
}
