﻿namespace Vouzamo.Pagination.Models.Interfaces
{
    /// <summary>
    /// Marker interface for generic pagination information
    /// </summary>
    public interface IPagination
    {
        #region Assigned Properties
        int TotalResults { get; }
        int ResultsPerPage { get; }
        int Page { get; }
        #endregion

        #region Calculated Properties
        int TotalPages { get; }
        int PreviousPage { get; }
        int NextPage { get; }
        bool HasPrevious { get; }
        bool HasNext { get; }
        #endregion
    }
}
