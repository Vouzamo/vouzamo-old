using System.Collections.Generic;
using System.Linq;
using Vouzamo.Common.Interop;

namespace Vouzamo.Pagination.Models.Interfaces
{
    public class SearchResultModel<TEntity, TViewModel> : IPaginatedSearchResult where TViewModel : IEditable
    {
        protected IMapper Mapper;
        protected readonly PagedResults<TEntity> _results;

        public string Keyword { get; private set; }
        public virtual IEnumerable<IEditable> Results
        {
            get { return _results.Results.Select(x => Mapper.Map<TEntity, TViewModel>(x) as IEditable); }
        }

        public int TotalResults { get { return _results.TotalResults; } }
        public int ResultsPerPage { get { return _results.ResultsPerPage; } }
        public int Page { get { return _results.Page; } }
        public int TotalPages { get { return _results.TotalPages; } }
        public int PreviousPage { get { return _results.PreviousPage; } }
        public int NextPage { get { return _results.NextPage; } }
        public bool HasPrevious { get { return _results.HasPrevious; } }
        public bool HasNext { get { return _results.HasNext; } }

        public SearchResultModel(IMapper mapper, string keyword, PagedResults<TEntity> results)
        {
            Mapper = mapper;
            Keyword = keyword;
            _results = results;
        }
    }
}