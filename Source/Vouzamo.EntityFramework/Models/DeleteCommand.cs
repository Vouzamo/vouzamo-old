﻿using System;
using Vouzamo.Command.Models;

namespace Vouzamo.EntityFramework.Models
{
    public class DeleteCommand<TEntity> : ICommand<bool>
    {
        public Guid Id { get; protected set; }

        public DeleteCommand(Guid id)
        {
            Id = id;
        }
    }
}
