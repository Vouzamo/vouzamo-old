using System;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Specification;

namespace Vouzamo.Blog.Data.Specifications
{
    public class AuthorPosts : Specification<Post>
    {
        protected Guid AuthorId { get; set; }

        public AuthorPosts(Guid authorId)
        {
            Predicate = x => x.AuthorId == authorId;
        }
    }
}