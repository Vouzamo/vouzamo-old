﻿namespace Vouzamo.Patterns.Builder
{
    /// <summary>
    /// Identifies a class that implements the builder pattern
    /// </summary>
    public interface IBuilder<out T>
    {
        T GetResult();
    }
}
