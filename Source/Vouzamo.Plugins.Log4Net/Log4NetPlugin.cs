﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using Vouzamo.Common.Plugins;

namespace Vouzamo.Plugins.Log4Net
{
    [ExportPlugin(typeof(ILogging), "66ba3960-2dbf-48cc-912a-023f24cae1d9", PluginAvailability.Public)]
    public class Log4NetPlugin : ILogging
    {
        protected List<LogLevel> LogLevels { get; set; }

        public Log4NetPlugin()
        {
            LogLevels = new List<LogLevel>();
            SetLogLevel(LogLevel.All);

            Configure();
        }

        public void Configure()
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

            PatternLayout patternLayout = new PatternLayout
            {
                ConversionPattern = "%date [%thread] %-5level %logger - %message%newline"
            };
            patternLayout.ActivateOptions();

            RollingFileAppender roller = new RollingFileAppender
            {
                AppendToFile = false,
                File = @"Logs\Log.txt",
                Layout = patternLayout,
                MaxSizeRollBackups = 5,
                MaximumFileSize = "1GB",
                RollingStyle = RollingFileAppender.RollingMode.Size,
                StaticLogFileName = true
            };
            roller.ActivateOptions();
            hierarchy.Root.AddAppender(roller);

            MemoryAppender memory = new MemoryAppender();
            memory.ActivateOptions();
            hierarchy.Root.AddAppender(memory);

            hierarchy.Root.Level = Level.All;
            hierarchy.Configured = true;
        }

        protected void SetLogLevel(LogLevel level)
        {
            LogLevels.Clear();

            switch (level)
            {
                case LogLevel.All:
                    goto case LogLevel.Debug;
                case LogLevel.Debug:
                    LogLevels.Add(LogLevel.Debug);
                    goto case LogLevel.Info;
                case LogLevel.Info:
                    LogLevels.Add(LogLevel.Info);
                    goto case LogLevel.Warn;
                case LogLevel.Warn:
                    LogLevels.Add(LogLevel.Warn);
                    goto case LogLevel.Error;
                case LogLevel.Error:
                    LogLevels.Add(LogLevel.Error);
                    goto case LogLevel.Fatal;
                case LogLevel.Fatal:
                    LogLevels.Add(LogLevel.Fatal);
                    break;
            }
        }

        public bool IsEnabled(LogLevel level)
        {
            bool enabled = true;

            if (level == LogLevel.All)
            {
                foreach (LogLevel l in Enum.GetValues(typeof(LogLevel)))
                {
                    if (l != LogLevel.Off && l != LogLevel.All)
                    {
                        enabled = enabled && IsEnabled(l);
                    }
                }
            }
            else
            {
                enabled = LogLevels.Contains(level);
            }

            return enabled;
        }

        public void Log(LogLevel level, string message, Exception ex = null)
        {
            if (IsEnabled(level))
            {
                StackFrame frame = new StackFrame(1);
                var method = frame.GetMethod();
                var type = method.DeclaringType;

                ILog logger = LogManager.GetLogger(type);

                if (logger != null)
                {
                    switch (level)
                    {
                        case LogLevel.All:
                        case LogLevel.Debug:
                            logger.Debug(message, ex);
                            break;
                        case LogLevel.Info:
                            logger.Info(message, ex);
                            break;
                        case LogLevel.Warn:
                            logger.Warn(message, ex);
                            break;
                        case LogLevel.Error:
                            logger.Error(message, ex);
                            break;
                        case LogLevel.Fatal:
                            logger.Fatal(message, ex);
                            break;
                    }
                }
            }
        }

        public void Dispose()
        {
            LogManager.Shutdown();
        }
    }
}
