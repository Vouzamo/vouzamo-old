﻿using NUnit.Framework;
using Vouzamo.Specification.Helpers;
using Vouzamo.Specification.Models.Interfaces;

namespace Vouzamo.Specification.Tests
{
    [TestFixture]
    public class SpecificationTests
    {
        #region Fields
        // Subject
        private readonly AlphaNumberic _subject = new AlphaNumberic('A', 1);

        // Specifications
        private readonly AlphaSpecification _alpha = new AlphaSpecification('A');
        private readonly NumericSpecification _numeric = new NumericSpecification(2);
        #endregion

        #region Tests
        [Test]
        public void PositiveTest()
        {
            Assert.IsTrue(_alpha.IsSatisfiedBy(_subject));
        }

        [Test]
        public void NegativeTest()
        {
            Assert.IsFalse(_numeric.IsSatisfiedBy(_subject));
        }

        [Test]
        public void AndTest()
        {
            Assert.IsFalse(_alpha.And(_numeric).IsSatisfiedBy(_subject));
        }

        [Test]
        public void OrTest()
        {
            Assert.IsTrue(_alpha.Or(_numeric).IsSatisfiedBy(_subject));
        }
        #endregion

        #region Internal Classes
        internal class AlphaNumberic
        {
            public char Alpha { get; private set; }
            public int Numeric { get; private set; }

            public AlphaNumberic(char alpha, int numeric)
            {
                Alpha = alpha;
                Numeric = numeric;
            }
        }

        internal class AlphaSpecification : ISpecification<AlphaNumberic>
        {
            public char Alpha { get; private set; }

            public AlphaSpecification(char alpha)
            {
                Alpha = alpha;
            }

            public bool IsSatisfiedBy(AlphaNumberic subject)
            {
                return subject.Alpha == Alpha;
            }
        }

        internal class NumericSpecification : ISpecification<AlphaNumberic>
        {
            public int Numeric { get; private set; }

            public NumericSpecification(int numeric)
            {
                Numeric = numeric;
            }

            public bool IsSatisfiedBy(AlphaNumberic subject)
            {
                return subject.Numeric == Numeric;
            }
        }
        #endregion
    }
}
