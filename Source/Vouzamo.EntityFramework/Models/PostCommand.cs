﻿using Vouzamo.Command.Models;
using Vouzamo.EntityFramework.Models.Interfaces;

namespace Vouzamo.EntityFramework.Models
{
    public class PostCommand<TEntity, TViewModel> : ICommand<TViewModel> where TViewModel : IEditable<TEntity>, new()
    {
        public TViewModel Model { get; protected set; }

        public PostCommand(TViewModel model)
        {
            Model = model;
        }
    }
}