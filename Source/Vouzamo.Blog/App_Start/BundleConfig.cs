﻿using System.Web.Optimization;

namespace Vouzamo.Blog
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/resources/scripts/admin").Include(
                "~/Static/Scripts/jquery-{version}.js",
                "~/Static/Scripts/bootstrap-{version}.js",
                "~/Static/Scripts/redactor-{version}.js"));

            bundles.Add(new ScriptBundle("~/resources/scripts/default").Include(
                "~/Static/Scripts/jquery-{version}.js",
                "~/Static/Scripts/bootstrap-{version}.js",
                "~/Static/Scripts/clean-blog-{version}.js",
                "~/Static/Scripts/prettify-{version}.js"));

            bundles.Add(new StyleBundle("~/resources/styles/admin").Include(
                "~/Static/Styles/CSS/bootstrap-{version}.css",
                "~/Static/Styles/CSS/redactor-{version}.css"));

            bundles.Add(new StyleBundle("~/resources/styles/default").Include(
                "~/Static/Styles/CSS/bootstrap-{version}.css",
                "~/Static/Styles/CSS/clean-blog-{version}.css",
                "~/Static/Styles/CSS/redactor-{version}.css",
                "~/Static/Styles/CSS/prettify-{version}.css",
                "~/Static/Styles/CSS/custom.css"));
        }
    }
}
