﻿using Vouzamo.Common.Interop;
using Vouzamo.Specification.Models.Interfaces;

namespace Vouzamo.EntityFramework.Models.Interfaces
{
    public interface IEditable<T> : IEditable
    {
        IOrderBySpecification<T> SearchSpecification(string keyword);
    }
}