﻿using System;

namespace Vouzamo.Common.Interop
{
    public interface IFactory<out T> where T : IDisposable
    {
        T Create();
    }
}
