using System.Data.Entity;
using System.Reflection;
using Vouzamo.Blog.Data;
using Vouzamo.Blog.Dispatchers;
using Vouzamo.Common.Data;

[assembly: WebActivator.PostApplicationStartMethod(typeof(Vouzamo.Blog.SimpleInjectorWebApiInitializer), "Initialize")]

namespace Vouzamo.Blog
{
    using System.Web.Http;
    using SimpleInjector;
    using SimpleInjector.Integration.WebApi;
    
    public static class SimpleInjectorWebApiInitializer
    {
        /// <summary>Initialize the container and register it as Web API Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
       
            container.Verify();
            
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
     
        private static void InitializeContainer(Container container)
        {
            container.Register<DbContext>(() => new BlogContext(), Lifestyle.Scoped);

            container.Register(typeof(ICommandHandler<,>), new[] { Assembly.GetExecutingAssembly() });
            container.Register(typeof(IQueryHandler<,>), new[] { Assembly.GetExecutingAssembly() });

            container.Register<ICommandDispatcher>(() => new SimpleInjectorCommandDispatcher(container));
            container.Register<IQueryDispatcher>(() => new SimpleInjectorQueryDispatcher(container));
        }
    }
}