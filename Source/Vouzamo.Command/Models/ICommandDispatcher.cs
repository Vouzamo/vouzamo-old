﻿namespace Vouzamo.Command.Models
{
    public interface ICommandDispatcher
    {
        TResult Invoke<TResult>(ICommand<TResult> command);
    }
}