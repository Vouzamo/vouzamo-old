﻿using System;
using System.Web.Mvc;
using Vouzamo.Blog.Data.Queries;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Controllers
{
    public class HomeController : Controller
    {
        protected IQueryDispatcher QueryDispatcher { get; set; }

        public HomeController(IQueryDispatcher queryDispatcher)
        {
            QueryDispatcher = queryDispatcher;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Posts(int id)
        {
            var results = QueryDispatcher.Invoke(new AllPostsQuery(id, 12));

            return View(results);
        }

        public ActionResult Post(Guid id)
        {
            var query = QueryDispatcher.Invoke(new PostByIdQuery(id));

            if (query.Successful)
            {
                return View(query.Response);
            }

            return HttpNotFound();
        }
    }
}
