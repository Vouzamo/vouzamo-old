﻿namespace Vouzamo.Data.InMemory
{
    public class InMemoryUnitOfWork// : IUnitOfWork
    {
    //    protected IList<Tuple<IComparable, IEntity<IComparable>>> Create { get; set; }
    //    protected IList<Tuple<IComparable, IEntity<IComparable>>> Update { get; set; }
    //    protected IList<Tuple<IComparable, IEntity<IComparable>>> Delete { get; set; }
    //    protected IList<Tuple<IComparable, IEntity<IComparable>>> Entities { get; set; }

    //    public InMemoryUnitOfWork()
    //    {
    //        Create = Enumerable.Empty<Tuple<IComparable, IEntity<IComparable>>>().ToList();
    //        Update = Enumerable.Empty<Tuple<IComparable, IEntity<IComparable>>>().ToList();
    //        Delete = Enumerable.Empty<Tuple<IComparable, IEntity<IComparable>>>().ToList();
    //        Entities = Enumerable.Empty<Tuple<IComparable, IEntity<IComparable>>>().ToList();
    //    }

    //    public IRepository<TEntity, TKey> Repository<TEntity, TKey>()
    //        where TEntity : class, IEntity<TKey>
    //        where TKey : IComparable
    //    {
    //        var repo = new InMemoryRepository<TEntity, TKey>(this);

    //        return repo;
    //    }

    //    public void Commit()
    //    {
    //        foreach (var entity in Create)
    //        {
    //            if (Entities.All(x => !Equals(x.Item2.Id, entity.Item2.Id)))
    //            {
    //                Entities.Add(entity);
    //            }
    //        }

    //        foreach (var entity in Update)
    //        {
    //            dynamic entityId = entity.Item2.Id;
    //            var entities = Entities.Where(x => x.Item2.Id == entityId).ToList();

    //            foreach (var remove in entities)
    //            {
    //                Entities.Remove(remove);
    //            }

    //            Entities.Add(entity);
    //        }

    //        foreach (var entity in Delete)
    //        {
    //            dynamic entityId = entity.Item2.Id;
    //            var entities = Entities.Where(x => x.Item2.Id == entityId).ToList();

    //            foreach (var remove in entities)
    //            {
    //                Entities.Remove(remove);
    //            }
    //        }

    //        Create.Clear();
    //        Update.Clear();
    //        Delete.Clear();
    //    }

    //    public void Rollback()
    //    {
    //        Create.Clear();
    //        Update.Clear();
    //        Delete.Clear();
    //    }

    //    public void Dispose()
    //    {
    //        // Do Nothing
    //    }

    //    private class InMemoryRepository<T, TKey> : IRepository<T, TKey>
    //        where T : class, IEntity<TKey>
    //        where TKey : IComparable
    //    {
    //        private InMemoryUnitOfWork UnitOfWork { get; set; }

    //        private IEnumerable<T> Entities
    //        {
    //            get
    //            {
    //                IList<T> entities = UnitOfWork.Entities.Where(x => x.Item1 is TKey && x.Item2 is T).Select(tuple => tuple.Item2 as T).ToList();

    //                return entities.ToList();
    //            }
    //        }

    //        public InMemoryRepository(InMemoryUnitOfWork unitOfWork)
    //        {
    //            UnitOfWork = unitOfWork;
    //        }

    //        public IRepositoryResponse<T> Find(TKey id)
    //        {
    //            //T entity = Entities.FirstOrDefault(x => x.Id == id);

    //            T entity = null;

    //            return new RepositoryResponse<T>(entity, entity != null);
    //        }

    //        public IRepositoryQueryForOneResponse<T> Query(IQueryForOne<T> query)
    //        {
    //            T entity = query.Process(Entities.AsQueryable());

    //            return new RepositoryQueryForOneResponse<T>(query, entity, entity != null);
    //        }

    //        public IRepositoryQueryForManyResponse<T> Query(IQueryForMany<T> query)
    //        {
    //            var entities = query.Process(Entities.AsQueryable());

    //            return new RepositoryQueryForManyResponse<T>(query, entities, entities.Any());
    //        }

    //        public IRepositoryResponse<T> Create(T entity)
    //        {
    //            UnitOfWork.Create.Add(entity);

    //            return new RepositoryResponse<T>(entity);
    //        }

    //        public IRepositoryResponse<T> Update(T entity)
    //        {
    //            UnitOfWork.Update.Add(entity);

    //            return new RepositoryResponse<T>(entity);
    //        }

    //        public IRepositoryResponse<T> Delete(T entity)
    //        {
    //            UnitOfWork.Delete.Add(entity);

    //            return new RepositoryResponse<T>(entity);
    //        }
    //    }
    //}

    //public class InMemoryRepository<T, TKey> : IRepository<T, TKey>
    //    where T : class, IEntity<TKey>
    //    where TKey : IComparable
    //{
    //    private InMemoryUnitOfWork UnitOfWork { get; set; }
    //    private IEnumerable<T> Entities { get { return UnitOfWork.Entities.OfType<T>().ToList(); } }

    //    public InMemoryRepository(InMemoryUnitOfWork unitOfWork)
    //    {
    //        UnitOfWork = unitOfWork;
    //    }

    //    public IRepositoryResponse<T> Find(TKey id)
    //    {
    //        //T entity = Entities.FirstOrDefault(x => x.Id == id);

    //        T entity = null;

    //        return new RepositoryResponse<T>(entity, entity != null);
    //    }

    //    public IRepositoryQueryForOneResponse<T> Query(IQueryForOne<T> query)
    //    {
    //        T entity = query.Process(Entities.AsQueryable());

    //        return new RepositoryQueryForOneResponse<T>(query, entity, entity != null);
    //    }

    //    public IRepositoryQueryForManyResponse<T> Query(IQueryForMany<T> query)
    //    {
    //        var entities = query.Process(Entities.AsQueryable());

    //        return new RepositoryQueryForManyResponse<T>(query, entities, entities.Any());
    //    }

    //    public IRepositoryResponse<T> Create(T entity)
    //    {
    //        UnitOfWork.Create.Add(new Tuple<IComparable, T>(entity.Id, entity)););

    //        return new RepositoryResponse<T>(entity);
    //    }

    //    public IRepositoryResponse<T> Update(T entity)
    //    {
    //        UnitOfWork.Update.Add(entity);

    //        return new RepositoryResponse<T>(entity);
    //    }

    //    public IRepositoryResponse<T> Delete(T entity)
    //    {
    //        UnitOfWork.Delete.Add(entity);

    //        return new RepositoryResponse<T>(entity);
    //    }
    }
}
