﻿namespace Vouzamo.Specification.Models.Interfaces
{
    public interface IThenBySpecification<T> : IOrderBySpecification<T>
    {

    }
}