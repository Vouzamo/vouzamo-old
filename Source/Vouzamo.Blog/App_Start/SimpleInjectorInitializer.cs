using System.Data.Entity;
using Vouzamo.Blog.Data;
using Vouzamo.Blog.Dispatchers;
using Vouzamo.Common.Data;

[assembly: WebActivator.PostApplicationStartMethod(typeof(Vouzamo.Blog.SimpleInjectorInitializer), "Initialize")]

namespace Vouzamo.Blog
{
    using System.Reflection;
    using System.Web.Mvc;

    using SimpleInjector;
    using SimpleInjector.Integration.Web;
    using SimpleInjector.Integration.Web.Mvc;
    
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            
            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            
            container.Verify();
            
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
     
        private static void InitializeContainer(Container container)
        {
            container.Register<DbContext>(() => new BlogContext(), Lifestyle.Scoped);

            container.Register(typeof(ICommandHandler<,>), new[] { Assembly.GetExecutingAssembly() });
            container.Register(typeof(IQueryHandler<,>), new[] { Assembly.GetExecutingAssembly() });

            container.Register<ICommandDispatcher>(() => new SimpleInjectorCommandDispatcher(container));
            container.Register<IQueryDispatcher>(() => new SimpleInjectorQueryDispatcher(container));
        }
    }
}