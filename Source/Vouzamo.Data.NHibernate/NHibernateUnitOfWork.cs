﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using Vouzamo.Common.Data;
using Vouzamo.Common.Interop;
using Vouzamo.Common.Specification;

namespace Vouzamo.Data.NHibernate
{
    //public abstract class NHibernateUnitOfWork : IUnitOfWork
    //{
    //    protected abstract ISession Session { get; }
    //    protected ITransaction Transaction { get; set; }

    //    protected NHibernateUnitOfWork()
    //    {
    //        Begin();
    //    }

    //    public IRepository<T,TKey> Repository<T,TKey>() where T : class, IEntity<TKey> where TKey : struct, IEquatable<TKey>
    //    {
    //        return new NHibernateRepository<T,TKey>(Session);
    //    }

    //    protected void Begin()
    //    {
    //        Transaction = Session.BeginTransaction();
    //    }

    //    public void Commit()
    //    {
    //        try
    //        {
    //            Transaction.Commit();
    //        }
    //        catch (Exception ex)
    //        {
    //            Rollback();
    //            throw;
    //        }
    //    }

    //    public void Rollback()
    //    {
    //        Transaction.Rollback();
    //    }

    //    public void Dispose()
    //    {
    //        Transaction.Dispose();
    //        Session.Dispose();
    //    }

    //    private class NHibernateRepository<T,TKey> : IRepository<T,TKey> where T : class, IEntity<TKey> where TKey : struct, IEquatable<TKey>
    //    {
    //        private ISession Session { get; set; }

    //        public NHibernateRepository(ISession session)
    //        {
    //            Session = session;
    //        }

    //        public ISingleResponse<T> Find(TKey id)
    //        {
    //            try
    //            {
    //                return new SingleResponse<T>(Session.Get<T>(id));
    //            }
    //            catch (Exception ex)
    //            {
    //                var response = new SingleResponse<T>();
    //                response.Exceptions.Add(ex);

    //                return response;
    //            }
    //        }

    //        public IManyResponse<T> Query(ISpecification<T> specification)
    //        {
    //            try
    //            {
    //                IList<T> results = specification.IsSatisfiedBy(Session.Query<T>()).ToList();

    //                return new ManyResponse<T>(results);
    //            }
    //            catch (Exception ex)
    //            {
    //                var response = new ManyResponse<T>();
    //                response.Exceptions.Add(ex);

    //                return response;
    //            }
    //        }

    //        public IManyResponse<T> Query(IOrderBySpecification<T> specification)
    //        {
    //            try
    //            {
    //                IList<T> results = specification.IsSatisfiedBy(Session.Query<T>()).ToList();

    //                return new ManyResponse<T>(results);
    //            }
    //            catch (Exception ex)
    //            {
    //                var response = new ManyResponse<T>();
    //                response.Exceptions.Add(ex);

    //                return response;
    //            }
    //        }

    //        public IManyPagedResponse<T> Query(IOrderBySpecification<T> specification, int resultsPerPage, int page = 1)
    //        {
    //            try
    //            {
    //                var queryable = specification.IsSatisfiedBy(Session.Query<T>());

    //                int totalResults = queryable.Count();
    //                int skip = (page - 1) * resultsPerPage;
    //                int take = resultsPerPage;

    //                IList<T> results = queryable.Skip(skip).Take(take).ToList();

    //                return new ManyPagedResponse<T>(results, totalResults, resultsPerPage, page);
    //            }
    //            catch (Exception ex)
    //            {
    //                var response = new ManyPagedResponse<T>(Enumerable.Empty<T>(), 0, resultsPerPage, page, false);
    //                response.Exceptions.Add(ex);

    //                return response;
    //            }
    //        }

    //        public ISingleResponse<T> Create(T entity)
    //        {
    //            try
    //            {
    //                Session.Save(entity);
    //            }
    //            catch (Exception ex)
    //            {
    //                var response = new SingleResponse<T>();
    //                response.Exceptions.Add(ex);

    //                return response;
    //            }

    //            return new SingleResponse<T>(entity);
    //        }

    //        public ISingleResponse<T> Update(T entity)
    //        {
    //            try
    //            {
    //                Session.Update(entity);
    //            }
    //            catch (Exception ex)
    //            {
    //                var response = new SingleResponse<T>();
    //                response.Exceptions.Add(ex);

    //                return response;
    //            }

    //            return new SingleResponse<T>(entity);
    //        }

    //        public ISingleResponse<T> Delete(T entity)
    //        {
    //            try
    //            {
    //                Session.Delete(entity);
    //            }
    //            catch (Exception ex)
    //            {
    //                var response = new SingleResponse<T>();
    //                response.Exceptions.Add(ex);

    //                return response;
    //            }

    //            return new SingleResponse<T>(entity);
    //        }
    //    }
    //}
}
