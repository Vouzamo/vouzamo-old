﻿using System;
using System.Reflection;
using Vouzamo.Common.Plugins;
using Vouzamo.Plugins;

namespace ExampleConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (var loggingPluginFactory = new PluginFactory<ILogging>(new PluginRepository<ILogging>("./Config"), new[] { "./" }))
                {
                    foreach (var plugin in loggingPluginFactory.All())
                    {
                        Console.WriteLine("Logging test message for: " + plugin.Metadata.Id);

                        using (var logger = plugin.Module)
                        {
                            logger.Log(LogLevel.Fatal, "test");
                        }

                        Console.ReadKey();
                    }
                }
            }
            catch (ReflectionTypeLoadException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
