using System;
using Vouzamo.Command.Models;
using Vouzamo.EntityFramework.Models.Interfaces;

namespace Vouzamo.EntityFramework.Models
{
    public class GetCommand<TEntity, TViewModel> : ICommand<TViewModel> where TViewModel : IEditable<TEntity>, new()
    {
        public Guid Id { get; protected set; }

        public GetCommand(Guid id)
        {
            Id = id;
        }
    }
}