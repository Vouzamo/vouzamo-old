﻿using System;
using System.Data.Entity;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Data.Commands
{
    public class CreatePostCommand : ICommand<Guid>
    {
        public string Title { get; protected set; }
        public string Summary { get; protected set; }
        public Guid AuthorId { get; protected set; }
        public Guid ImageId { get; protected set; }

        public CreatePostCommand(string title, string summary, Guid authorId, Guid imageId)
        {
            Title = title;
            Summary = summary;
            AuthorId = authorId;
            ImageId = imageId;
        }
    }

    public class CreatePostCommandHandler : ICommandHandler<CreatePostCommand, Guid>
    {
        public DbContext Context { get; set; }

        public CreatePostCommandHandler(DbContext context)
        {
            Context = context;
        }

        public Guid Execute(CreatePostCommand command)
        {
            try
            {
                Post post = new Post(command.Title, command.Summary, command.AuthorId, command.ImageId, DateTime.MinValue);

                Context.Entry(post).State = EntityState.Added;
                Context.SaveChanges();

                return post.Id;
            }
            catch (Exception ex)
            {
                return Guid.Empty;
            }
        }
    }

    public class ChangePostImageCommand : ICommand<bool>
    {
        public Guid PostId { get; protected set; }
        public Guid ImageId { get; protected set; }

        public ChangePostImageCommand(Guid postId, Guid imageId)
        {
            PostId = postId;
            ImageId = imageId;
        }
    }

    public class PublishPostCommand : ICommand<bool>
    {
        
    }

    public class AppendSectionCommand : ICommand<bool>
    {
        public Section Section { get; set; }
    }

    public class InsertSectionCommand : ICommand<bool>
    {
        public Section Section { get; set; }
        public int Index { get; set; }
    }

    public class CreateAuthorCommand : ICommand<bool>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class ChangeAuthorNameCommand : ICommand<bool>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class CreateImageCommand : ICommand<bool>
    {
        public string Url { get; set; }
        public string Alt { get; set; }
    }
}