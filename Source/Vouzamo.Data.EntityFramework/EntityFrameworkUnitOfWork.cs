﻿using System.Data.Entity;
using System.Linq;

namespace Vouzamo.Data.EntityFramework
{
    public abstract class EntityFrameworkQueryHandler<TResult> where TResult : class
    {
        private readonly DbContext _context;
        protected IQueryable<TResult> Data { get { return _context.Set<TResult>(); } }

        protected EntityFrameworkQueryHandler(DbContext context)
        {
            _context = context;
        }
    }
}
