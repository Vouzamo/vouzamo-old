﻿using System;
using System.Web.Mvc;
using Vouzamo.Blog.Data.Queries;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Controllers
{
    public class AuthorController : Controller
    {
        protected IQueryDispatcher QueryDispatcher { get; set; }

        public AuthorController(IQueryDispatcher queryDispatcher)
        {
            QueryDispatcher = queryDispatcher;
        }

        public ActionResult AuthorTagline(Guid id)
        {
            var query = QueryDispatcher.Invoke(new AuthorByIdQuery(id));

            return View(query.Response);
        }
    }
}