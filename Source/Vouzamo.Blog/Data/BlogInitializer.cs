using System;
using System.Data.Entity;
using Vouzamo.Blog.Models.Domain;

namespace Vouzamo.Blog.Data
{
    public class BlogInitializer : DropCreateDatabaseIfModelChanges<BlogContext>
    {
        protected override void Seed(BlogContext context)
        {
            base.Seed(context);

            Image image = new Image("http://thecatapi.com/api/images/get?format=src&type=gif&size=full", "Random Cat Image");
            context.Entry(image).State = EntityState.Added;

            Author author = new Author("John", "Askew");
            context.Entry(author).State = EntityState.Added;

            Post post = new Post("First Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2013, 5, 24));
            context.Entry(post).State = EntityState.Added;

            context.Entry(new TextSection(post.Id, 10, "This is an example blog post to test the blog application code")).State = EntityState.Added;
            context.Entry(new HeadingSection(post.Id, 20, "Section Types")).State = EntityState.Added;
            context.Entry(new TextSection(post.Id, 30, "A blog post can have many sections of different types. Types currently include: Heading, Text, Code, and Quote. I will add additional types for: Image, Video etc too. A code section is shown below...")).State = EntityState.Added;
            context.Entry(new CodeSection(post.Id, 40, "<ul>\n\t<li>\n\t\tMorbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.\n\t</li>\n\t<li>\n\t\tPraesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.\n\t</li>\n\t<li>\n\t\tPhasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.\n\t</li>\n\t<li>\n\t\tPellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.\n\t</li>\n</ul>")).State = EntityState.Added;
            context.Entry(new TextSection(post.Id, 50, "Here is a quote example...")).State = EntityState.Added;
            context.Entry(new QuoteSection(post.Id, 60, "To be or not to be. That is the question.", "William Shakespeare", "Memory")).State = EntityState.Added;
            
            context.Entry(new Post("Second Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 5, 24))).State = EntityState.Added;
            context.Entry(new Post("Third Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2015, 5, 24))).State = EntityState.Added;

            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;
            context.Entry(new Post("Example Post", "Introduction Paragraph", author.Id, image.Id, new DateTime(2014, 12, 25))).State = EntityState.Added;

            context.Entry(new Witticism("Oct 31 == Dec 25", "https://www.google.com/search?q=Oct 31 == Dec 25")).State = EntityState.Added;
            context.Entry(new Witticism("There are 10 types of people in this world, those who understand binary and those who don't.", "http://www.urbandictionary.com/define.php?term=there+are+10+types+of+people+in+this+world%2C+those+who+understand+binary+and+those+who+dont")).State = EntityState.Added;

            context.SaveChanges();
        }
    }
}