using System;
using Microsoft.Practices.Unity;
using SimpleInjector;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Dispatchers
{
    public class SimpleInjectorQueryDispatcher : IQueryDispatcher
    {
        protected Container Container { get; set; }

        public SimpleInjectorQueryDispatcher(Container container)
        {
            Container = container;
        }

        public TResult Invoke<TResult>(IQuery<TResult> query)
        {
            Type queryHandlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));

            // Resolve
            dynamic handler = Container.GetInstance(queryHandlerType);

            // Execute
            return handler.Execute((dynamic)query);
        }
    }

    public class UnityQueryDispatcher : IQueryDispatcher
    {
        protected IUnityContainer Container { get; set; }

        public UnityQueryDispatcher(IUnityContainer container)
        {
            Container = container;
        }

        public TResult Invoke<TResult>(IQuery<TResult> query)
        {
            // Resolve
            IQueryHandler<IQuery<TResult>, TResult> handler = Container.Resolve<IQueryHandler<IQuery<TResult>, TResult>>();

            // Execute
            return handler.Execute(query);
        }
    }
}