﻿using System.Collections.Generic;
using System.Linq;
using Vouzamo.Pagination.Models;

namespace Vouzamo.Pagination.Helpers
{
    public static class PaginationHelpers
    {
        /// <summary>
        /// Helper method to convert an IOrderedQueryable{T} to a PagedResults{T}
        /// </summary>
        /// <typeparam name="TResult">The concrete type of the results</typeparam>
        /// <param name="orderedQueryable">IOrderedQueryable{TResult} that needs to be paged</param>
        /// <param name="resultsPerPage">Number of results per page</param>
        /// <param name="page">1 based index of the desired page</param>
        /// <returns></returns>
        public static PagedResults<TResult> ToPagedResults<TResult>(this IOrderedQueryable<TResult> orderedQueryable, int resultsPerPage, int page = 1)
        {
            // Database call to get the total result count
            int totalResultCount = orderedQueryable.Count();

            int skip = ((page - 1) * resultsPerPage);

            // Database call to get the requested page of results
            List<TResult> results = orderedQueryable.Skip(skip).Take(resultsPerPage).ToList();

            return new PagedResults<TResult>(results, totalResultCount, resultsPerPage, page);
        }
    }
}
