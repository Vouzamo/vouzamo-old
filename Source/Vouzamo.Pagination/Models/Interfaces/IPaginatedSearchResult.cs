﻿using System.Collections.Generic;
using Vouzamo.Common.Interop;

namespace Vouzamo.Pagination.Models.Interfaces
{
    /// <summary>
    /// Marker interface to combine ISearch and IPagination
    /// </summary>
    public interface IPaginatedSearchResult : ISearch, IPagination
    {
        IEnumerable<IEditable> Results { get; }
    }
}