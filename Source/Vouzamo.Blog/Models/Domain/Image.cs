﻿using System;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Models.Domain
{
    public class Image : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string AlternateText { get; set; }

        public Image()
        {
            Id = Guid.NewGuid();
        }

        public Image(string url, string alternateText) : this()
        {
            Url = url;
            AlternateText = alternateText;
        }
    }
}