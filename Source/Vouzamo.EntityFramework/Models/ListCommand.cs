﻿using Vouzamo.Command.Models;
using Vouzamo.EntityFramework.Models.Interfaces;
using Vouzamo.Pagination.Models.Interfaces;

namespace Vouzamo.EntityFramework.Models
{
    public class ListCommand<TEntity, TViewModel> : ICommand<SearchResultModel<TEntity, TViewModel>> where TViewModel : IEditable<TEntity>, new()
    {
        public string Keyword { get; protected set; }
        public int Page { get; protected set; }
        public int ResultsPerPage { get; protected set; }

        public ListCommand(string keyword, int page = 1, int resultsPerPage = 10)
        {
            Keyword = keyword;
            Page = page;
            ResultsPerPage = resultsPerPage;
        }
    }
}