﻿namespace Vouzamo.Patterns.Factory
{
    /// <summary>
    /// Identifies a class that implements the factory pattern
    /// </summary>
    /// <typeparam name="T">The type of object which this factory can create</typeparam>
    public interface IFactory<T>
    {
        /// <summary>
        /// Creates a concrete implementation
        /// </summary>
        /// <typeparam name="TConcrete">The concrete type to instantiate</typeparam>
        /// <returns>Instantiated object</returns>
        T Create<TConcrete>() where TConcrete : T; 
    }
}
