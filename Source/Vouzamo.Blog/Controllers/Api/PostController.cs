﻿using System;
using System.Web.Http;
using Vouzamo.Blog.Data.Queries;
using Vouzamo.Blog.Models.Domain;
using Vouzamo.Common.Data;
using Vouzamo.Common.Interop;

namespace Vouzamo.Blog.Controllers.Api
{
    [RoutePrefix("api/post")]
    public class PostsApiController : ApiController
    {
        protected IQueryDispatcher QueryDispatcher { get; set; }

        public PostsApiController(IQueryDispatcher queryDispatcher)
        {
            QueryDispatcher = queryDispatcher;
        }

        [Route("page/{page}"), HttpGet]
        public IManyPagedResponse<Post> Posts(int page)
        {
            return QueryDispatcher.Invoke(new AllPostsQuery(page, 12));
        }

        // This would be better on the Author controller e.g. /author/{id}/posts
        [Route("author/{authorId}/{page}"), HttpGet]
        public IManyResponse<Post> PostsByAuthor(Guid authorId, int page)
        {
            return QueryDispatcher.Invoke(new PostsByAuthorIdQuery(authorId, page, 12));
        }

        [Route("{id}"), HttpGet]
        public ISingleResponse<Post> Post(Guid id)
        {
            return QueryDispatcher.Invoke(new PostByIdQuery(id));
        }

        [Route("{id}/sections"), HttpGet]
        public IManyResponse<Section> PostSections(Guid id)
        {
            return QueryDispatcher.Invoke(new AllSectionsQuery(id));
        }
    }
}
