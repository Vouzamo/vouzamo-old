﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Vouzamo.Pagination.Helpers;
using Vouzamo.Pagination.Models;
using Vouzamo.Specification.Models.Interfaces;

namespace Vouzamo.EntityFramework.Helpers
{
    public static class DbSetHelpers
    {
        public static IEnumerable<T> QueryBySpecification<T>(this DbSet<T> dbSet, IWhereSpecification<T> specification) where T : class
        {
            return specification.SatisfiesMany(dbSet).ToList();
        }

        public static PagedResults<T> PaginatedQueryBySpecification<T>(this DbSet<T> dbSet, IOrderBySpecification<T> specification, int resultsPerPage, int page = 1) where T : class
        {
            return specification.SatisfiesMany(dbSet).ToPagedResults(resultsPerPage, page);
        }
    }
}
