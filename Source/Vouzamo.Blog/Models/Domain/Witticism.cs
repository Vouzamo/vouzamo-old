﻿using System;
using Vouzamo.Common.Data;

namespace Vouzamo.Blog.Models.Domain
{
    public class Witticism : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public string Source { get; set; }
        public string Url { get; set; }

        public Witticism()
        {
            Id = Guid.NewGuid();
        }

        public Witticism(string text, string url, string source = "") : this()
        {
            Text = text;
            Url = url;
            Source = source;
        }
    }
}